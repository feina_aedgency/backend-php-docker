FROM ubuntu:16.04
MAINTAINER "Carles" <cejarque@aedgency.com> version: 0.1a

# Prepare Debian environment
ENV DEBIAN_FRONTEND noninteractive
# we don't need an apt cache in a container
RUN echo "Acquire::http {No-Cache=True;};" > /etc/apt/apt.conf.d/no-cache

RUN apt-get update > /dev/null 2>&1 && \
    apt-get upgrade -y > /dev/null 2>&1

ADD files/.ssh/id_rsa /root/.ssh/id_rsa
ADD files/.ssh/id_rsa.pub /root/.ssh/id_rsa.pub
ADD files/.ssh/config /root/.ssh/config
RUN chmod 0600 /root/.ssh/id_rsa                                   && \
    chmod 0600 /root/.ssh/config

## Install utils
RUN apt-get install -y vim telnet wget > /dev/null 2>&1

## Instaling Apache.
RUN apt-get install -y apache2 ca-certificates > /dev/null 2>&1     && \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf        && \
    a2enmod rewrite ssl proxy proxy_fcgi > /dev/null 2>&1

## Instaling PHP 7.
RUN apt-get install -y php7.0 php7.0-cli php7.0-json php7.0-mbstring php7.0-zip php-xml php7.0-gd php7.0-sqlite3 \
                        php7.0-mcrypt php7.0-imap php7.0-ldap php7.0-curl php7.0-readline php-imagick \
                        php-apcu php-xdebug php7.0-dev > /dev/null 2>&1

# Config php-fpm
RUN	sed -i "s|;date.timezone =|date.timezone = UTC|" /etc/php/7.0/fpm/php.ini && \
	sed -i "s|listen = /run/php/php7.0-fpm.sock|;listen = /run/php/php7.0-fpm.sock|" /etc/php/7.0/fpm/pool.d/www.conf && \
	sed -i "s|;listen = /run/php/php7.0-fpm.sock|;listen = /run/php/php7.0-fpm.sock\nlisten = 9000|" /etc/php/7.0/fpm/pool.d/www.conf && \
	sed -i "s|;env|env|" /etc/php/7.0/fpm/pool.d/www.conf && \
	sed -i "s|;error_log = php_errors.log|error_log = /var/log/php_errors.log|" /etc/php/7.0/fpm/php.ini

## Xdebug
RUN mkdir /tmp/xdebug
RUN cd /tmp/xdebug                                                                  && \
    wget "http://xdebug.org/files/xdebug-2.5.0.tgz" > /dev/null 2>&1                && \
    tar -xvzf xdebug-2.5.0.tgz > /dev/null 2>&1                                     && \
    cd xdebug-2.5.0                                                                 && \
    phpize > /dev/null 2>&1                                                         && \
    ./configure > /dev/null 2>&1                                                    && \
    make > /dev/null 2>&1                                                           && \
    cp modules/xdebug.so /usr/lib/php/20151012/
RUN echo "zend_extension = /usr/lib64/php/7.0/modules/xdebug.so" >> /etc/php.ini

RUN echo >> /etc/php/7.0/mods-available/xdebug.ini                                   && \
    echo "[XDEBUG]" >> /etc/php/7.0/mods-available/xdebug.ini                        && \
    echo "xdebug.default_enable=1" >> /etc/php/7.0/mods-available/xdebug.ini         && \
    echo "xdebug.remote_autostart=1" >> /etc/php/7.0/mods-available/xdebug.ini       && \
    echo "xdebug.remote_connect_back=1" >> /etc/php/7.0/mods-available/xdebug.ini    && \
    echo "xdebug.remote_enable=1" >> /etc/php/7.0/mods-available/xdebug.ini          && \
    echo "xdebug.remote_handler=dbgp" >> /etc/php/7.0/mods-available/xdebug.ini      && \
    echo "xdebug.remote_port=9000" >> /etc/php/7.0/mods-available/xdebug.ini         && \
    echo "xdebug.remote_mode=req" >> /etc/php/7.0/mods-available/xdebug.ini          && \
    echo "xdebug.idekey=PHPStorm" >> /etc/php/7.0/mods-available/xdebug.ini

## Composer.
RUN apt-get install -y ca-certificates curl > /dev/null 2>&1
## Setup Composer
RUN curl -sS https://getcomposer.org/installer | php  > /dev/null 2>&1  && \
	mv composer.phar /usr/local/bin/composer                            && \
    composer self-update > /dev/null 2>&1

RUN mkdir -p /root/.config/composer                                                                                     && \
    echo { > /root/.config/composer/auth.json                                                                           && \
    echo \  \"github-oauth\": { >> /root/.config/composer/auth.json                                                     && \
    echo \ \ \ \ \ \ \"github.com\": \"d5b7488bd585a416937f34cf95215d9ddc4b08b2\" >> /root/.config/composer/auth.json   && \
    echo \ \ \ } >> /root/.config/composer/auth.json                                                                    && \
    echo } >> /root/.config/composer/auth.json                                                                          && \
    mkdir -p /root/.composer                                                                                            && \
    cp -Rp /root/.config/composer/* /root/.composer/

## For Yii2
RUN composer global require "fxp/composer-asset-plugin:^1.3.1" > /dev/null 2>&1
## ## End Composer.

## Git
RUN apt-get install -y git bash-completion > /dev/null 2>&1     && \
    git config --global user.email "Tester@aedgency.com"      && \
    git config --global user.name "Tester"

## Memcached
RUN apt-get install -y memcached > /dev/null 2>&1
# http://www.memcached.org/files/memcached-1.5.0.tar.gz
RUN git clone https://Carles74@bitbucket.org/verdenegro/phpmemcachedadmin.git /opt/phpMemcachedAdmin > /dev/null 2>&1   && \
    chmod 0777 /opt/phpMemcachedAdmin/Config/Memcache.php                                                               && \
    chmod 0777 /opt/phpMemcachedAdmin/Temp/                                                                             && \
    sed -i "s|'127.0.0.1:11211'|'172.17.0.2:11211'|" /opt/phpMemcachedAdmin/Config/Memcache.php                                && \
    sed -i "s|'hostname' => '127.0.0.1'|'hostname' => '172.17.0.2'|" /opt/phpMemcachedAdmin/Config/Memcache.php

## APC Admin2
# https://pecl.php.net/get/APC-3.1.13.tgz
RUN mkdir /opt/APC-Admin2/                                              && \
    wget -O- "https://raw.githubusercontent.com/SegFaulty/php-apcu-bc/master/ApcApcuCompat.php"  >> /opt/APC-Admin2/ApcApcuCompat.php 2> /dev/null
ADD files/apc/apc.php /opt/APC-Admin2/index.php
ADD files/apc/apc.conf.php /opt/APC-Admin2/apc.conf.php
RUN sed -i "s|$cache_mode='opcode';|$cache_mode='user';|" /opt/APC-Admin2/index.php    && \
    chown -R www-data: /opt/APC-Admin2

## PhantomJS
RUN apt-get install -y wget bzip2 > /dev/null 2>&1
RUN mkdir /tmp/phantom
RUN cd /tmp/phantom
RUN wget -U 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.6) Gecko/20070802 SeaMonkey/1.1.4' "https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2" > /tmp/ph.txt 2>&1
RUN tar jxvf phantomjs-2.1.1-linux-x86_64.tar.bz2 > /tmp/tar.txt 2>&1
RUN mv phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/
RUN /usr/local/bin/phantomjs --webdriver=4444 --ignore-ssl-errors=true --ssl-protocol=any > /var/log/phantomjs.log 2>&1 &

## Bashrc
RUN apt-get install -y wget > /dev/null 2>&1
RUN echo ". /usr/lib/git-core/git-sh-prompt" >> /root/.bashrc 2> /dev/null
RUN wget -O- "https://bitbucket.org/verdenegro/fitxersauxiliars/raw/b90af3ad346cd001a2eb15b4f60725dd075f0232/.bashrc-ext-ps1" >> /root/.bashrc 2> /dev/null

# Delete Tmp Files
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN apt-get clean

ADD files/apache2/10-test_ssl.conf /etc/apache2/sites-enabled/10-test_ssl.conf
ADD files/apache2/20-memcached_ssl.conf /etc/apache2/sites-enabled/20-memcached_ssl.conf
ADD files/apache2/20-apc_ssl.conf /etc/apache2/sites-enabled/20-apc_ssl.conf
#RUN service php7.0-fpm start > /dev/null 2>&1
#RUN service apache2 start > /dev/null 2>&1

## App
RUN git clone git@bitbucket.org:feina_aedgency/backend-php-skeleton.git /var/www/html2 > /dev/null 2>&1                 && \
    mv /var/www/html /var/www/html3                                                                                     && \
    mv /var/www/html2 /var/www/html                                                                                     && \
    cd /var/www/html/                                                                                                   && \
    composer install > /dev/null 2>&1                                                                                   && \
    composer update > /dev/null 2>&1                                                                                    && \
    php init --env=Production --overwrite=All > /dev/null 2>&1                                                          && \
    php yii migrate --interactive=0 > /dev/null 2>&1                                                                    && \
    chown -R www-data: *                                                                                                && \
    chown -R www-data: .*

EXPOSE 80 443
ADD files/run.sh /run.sh
RUN chmod 0755 /run.sh
CMD ["bash", "run.sh"]