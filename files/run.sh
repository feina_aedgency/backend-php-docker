#!/usr/bin/env bash

# Xdebug
echo "xdebug.remote_host=`ip route |head -n1| awk '{print $3}' | sed 's/addr://'`" >> /etc/php-7.0.d/xdebug.ini

## Memcached
memcached -p 11211 -d -u memcache

## PhantomJS
/usr/local/bin/phantomjs --webdriver=4444 --ignore-ssl-errors=true --ssl-protocol=any > /var/log/phantomjs.log 2>&1 &

# Start services
service rsyslog start > /dev/null 2>&1
service crond start > /dev/null 2>&1
service php7.0-fpm start
service apache2 start

# To mantain alive
/bin/bash