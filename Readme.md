## How to Install
```
$ git clone https://bitbucket.org/feina_aedgency/backend-php-docker.git
$ cd backend-php-docker
$ docker build --rm --memory="1GB" --memory-swap="-1" -t test .
$ docker run -t -d -v test
$ echo "172.17.0.2 test.aedgency.int memcached.aedgency.int apc.aedgency.int" >> /etc/hosts"
``` 

Then, you can run your test [website](https://test.aedgency.int), [memcached](https://memcached.aedgency.int) and [apc](https://apc.aedgency.int).

You can edit your code by use
```
$ git clone git@your/repository.git ~/path/to/your/repository/
$ docker stop `docker ps |awk '{print $1}'` 
$ docker run -t -d -v ~/path/to/your/repository/:/var/www/html test
```

Aedgency PHP Backend test
=========================

##Design and implementation the test
The owner of a store for food and accessories for animals needs a web application to have control of the stock and sales made.

The user of the application will initially access to a page with a small dashboard with the list of the latest orders, as well as the option of managing the products, customers and orders:  
- Products. A list of all products, with the option to add new products and divided into categories. List the product id, the name of the product, its category and a link to view and / or edit the product.

- Customers. A list of customers. Option to add and edit customers. You need to list at least the name and if for each of them.

- Order. A list of orders, with the customer, the transaction date and the number of the products selled.

We need a system to export to memcache the data to use in the dashboard. This process will be done every X minutes.
Some actions must be implemented as a call to an API REST.

###We need:  
1. The data desing.  
2. The model design.  
3. Some products, clients and orders to test.  

###About the test
- You can use Yii2, laravel or other framework to do the test.
- We wants to see how you use APIs, caches or a framework.
- The docker container it's only a tool, it's not required. If you want to use a lamp, wamp or environment, you can. Only send to us some intructions to test your code.
- The test is for backend, no great views are needed.
- You can use different technologies, it's your time to show to us what you know ;)
